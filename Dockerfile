FROM alpine:3.10

# Install PHP7 from the alpine repository (currently 7.3)
RUN apk add --update --no-cache \
    coreutils \
    php7-fpm \
    php7-apcu \
    php7-ctype \
    php7-curl \
    php7-dom \
    php7-gd \
    php7-iconv \
    php7-imagick \
    php7-json \
    php7-intl \
    php7-mcrypt \
    php7-fileinfo\
    php7-mbstring \
    php7-opcache \
    php7-openssl \
    php7-pdo \
    php7-pdo_mysql \
    php7-mysqli \
    php7-xml \
    php7-zlib \
    php7-phar \
    php7-tokenizer \
    php7-session \
    php7-simplexml \
    php7-xdebug \
    php7-zip \
    php7-xmlwriter \
    php7-xmlreader \
    make \
    curl

# Download composer and make it available in /usr/bin/composer
RUN echo "$(curl -sS https://composer.github.io/installer.sig) -" > composer-setup.php.sig \
    && curl -sS https://getcomposer.org/installer | tee composer-setup.php | sha384sum -c composer-setup.php.sig \
    && php composer-setup.php && rm composer-setup.php* \
    && chmod +x composer.phar && mv composer.phar /usr/bin/composer

# Copy our custom php config files
COPY php-config.ini /etc/php7/conf.d/
COPY php-config.ini /etc/php7/cli/conf.d/
COPY php-xdebug.ini  /etc/php7/conf.d/

# Copy custom php-fpm pool config
COPY symfony.pool.conf /etc/php7/php-fpm.d/

# Caddy setup/install
RUN curl -sSL "https://github.com/caddyserver/caddy/releases/download/v2.0.0-rc.3/caddy_2.0.0-rc.3_linux_amd64.tar.gz" | tar -xz \
    && mv caddy /usr/bin/caddy \
    && chmod +x /usr/bin/caddy

# Copy our Caddy configuration
COPY Caddyfile /etc/caddy/Caddyfile

# Copy our contao files into the container
COPY app /srv/app
COPY config /srv/config
COPY contao /srv/contao
COPY src /srv/src
COPY [".env", "composer.json", "composer.lock", "/srv/"]
COPY web/.htaccess /srv/web/
# add our startup script
COPY bootstrap.sh /

# make the startup script executable
RUN chmod +x /bootstrap.sh
# run composer install on container build
RUN cd /srv && composer install

# set default values for environment variables
ENV SITE_ADDRESS=localhost
ENV DEVMODE='False'

# Register volumes
VOLUME [ "/srv", "/srv/files", "/srv/var/logs", "/srv/assets" ]

# Set boostrap.sh as entrypoint
CMD ["/bootstrap.sh"]

WORKDIR /srv
EXPOSE 80 443