#!/bin/sh
if [ "$DEVMODE" = "True" ]; then
  echo "Dev mode is active!"
  mkdir -p /var/log/php7
  echo 'alias phprun="su dev -c"' >> /root/.profile
  # add a dev user to run composer under when needed
  adduser -DH -u 1000 dev
  # install contao and dependencies
  su dev -c "composer install"
fi

# make sure symlinks are intact after volumes have been mounted
vendor/bin/contao-console contao:symlinks
# Clear the contao cache before runtime startup and after volumes have
# been mounted
vendor/bin/contao-console cache:clear
# make sure permissions are set
chown -R 1000:1000 /srv

# Start the caddy server process
caddy start --config /etc/caddy/Caddyfile --adapter caddyfile --watch
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start caddy: $status"
  exit $status
fi

# Start the PHP-FPM process
php-fpm7
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start php-fpm: $status"
  exit $status
fi

# Naive check runs checks once a minute to see if either of the processes exited.
# This illustrates part of the heavy lifting you need to do if you want to run
# more than one service in a container. The container exits with an error
# if it detects that either of the processes has exited.
# Otherwise it loops forever, waking up every 60 seconds

while sleep 60; do
  ps aux |grep caddy |grep -q -v grep
  PROCESS_1_STATUS=$?
  ps aux |grep php-fpm7 |grep -q -v grep
  PROCESS_2_STATUS=$?
  # If the greps above find anything, they exit with 0 status
  # If they are not both 0, then something is wrong
  if [ $PROCESS_1_STATUS -ne 0 -o $PROCESS_2_STATUS -ne 0 ]; then
    echo "One of the processes has already exited."
    exit 1
  fi
done