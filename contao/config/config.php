<?php

/**
 * Disable template editor and settings
 */
unset($GLOBALS['BE_MOD']['design']['tpl_editor'],
$GLOBALS['BE_MOD']['system']['settings']);
